const restify = require('restify');
const{BotFrameworkAdapter, ConversationState, MemoryStorage, UserState }= require('botbuilder');

const { RichCardsBot } = require('./bots/richCardsBot');
const { MainDialog } = require('./dialogs/mainDialog');

//adapter

const adapter = new BotFrameworkAdapter({
    appID :' ',
    appPassword : ' '

});

adapter.onTurnError = async(context,error) => {
    console.log("error occured", error);
    await context.sendActivity('Bot encountered an error');

};

const memoryStorage = new MemoryStorage();
const conversationState = new ConversationState(memoryStorage);
const userState = new UserState(memoryStorage);

// Create the main dialog.
const dialog = new MainDialog();
const bot = new RichCardsBot(conversationState, userState, dialog);



let server = restify.createServer();
server.listen(3978, ()=>{
        console.log(`${server.name} listening to ${server.url}`);
    });
    
server.post('/api/messages', (req, res) => {
        adapter.processActivity(req, res, async (context) => {
            await bot.run(context);
        });
    });    
