const { MessageFactory } = require('botbuilder');
const { DialogBot } = require('./dialogBot');


class RichCardsBot extends DialogBot {
    constructor(conversationState, userState, dialog) {
        super(conversationState, userState, dialog);

        this.onMembersAdded(async (context, next) => {
            const membersAdded = context.activity.membersAdded;
            for (let cnt = 0; cnt < membersAdded.length; cnt++) {
                if (membersAdded[cnt].id !== context.activity.recipient.id) {
                    const reply = MessageFactory.text('Welcome to CardBot. ' +
                        'This bot will show you different types of Rich Cards. ' +
                        'Please type anything to get started.');
                    await context.sendActivity(reply);
                }
            }

            await next();
        });
    }
}

module.exports.RichCardsBot = RichCardsBot;